import request from 'supertest';
import app from '../src/index'; // Import the express app

describe('GET /', () => {
  it('should return the correct response', async () => {
    const response = await request(app).get('/');
    expect(response.status).toBe(200);
    expect(response.text).toContain('Express + TypeScript Server. Timestamp:');

    process.exit();
  });
});