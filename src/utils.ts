export function getTimestamp(): string {
    const date = new Date();
    return date.toISOString();
  }